FROM ocaml/opam:alpine-3.19-ocaml-5.1

RUN sudo apk add linux-headers

ADD --chown=opam:opam zimmerman.opam /src/zimmerman.opam
WORKDIR /src
RUN opam install . --deps-only --with-test

ADD --chown=opam:opam . /src
RUN eval $(opam env) && dune build .

ENTRYPOINT [ "/src/_build/default/bin/test.exe" ]