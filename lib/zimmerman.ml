(*
 * An Eio web framework using http/af
 * Zimmerman implements a cascading middleware system (equipped to take advantage of Eio's cancellation mechanisms)
 *)

open Eio
open! Core
open Printf

module String = CCString
module Typed_router = Typed_router
module Content_type = Content_type
module Core = Core
type request = Core.request
type response = Core.response

exception User_disconnect = Core.User_disconnect
exception Bad_request = Core.Bad_request
exception Http_not_found = Core.Http_not_found

(* Stops switches from expiring during streaming requests *)
let switch_with_req_lifetime ?name req fn =
  Switch.run ?name (fun sw ->
    (* hack to keep the switch alive for the full duration of the request *)
    Fiber.fork ~sw (fun () -> Promise.await req.body_finished);

    fn {req with sw}
  )


(* Builtin middlewares *)

(* format: [STATUS] [TIME] [ADDRESS]
 * RST = Connection reset
 *)
let logger _ next (req, res) =
  let req, res = next (req, res) in
  Logs.info (fun m -> m "%s \x1b[105m%fs\x1b[0m %s %s" (
    let format =
      if Httpaf.Status.is_server_error res.status then sprintf "\x1b[41m%s\x1b[0m"
      else if Httpaf.Status.is_client_error res.status then (
        if res.status = `I_m_a_teapot then
          (fun _ -> "\x1b[43mRST\x1b[0m")
        else
          sprintf "\x1b[43m%s\x1b[0m"
      )
      else if Httpaf.Status.is_redirection res.status then sprintf "\x1b[46m%s\x1b[0m"
      else sprintf "\x1b[42m%s\x1b[0m"
    in
    
    format (Httpaf.Status.to_string res.status)
  ) (Unix.gettimeofday () -. req.time) (Httpaf.Method.to_string req.meth) req.resource);
  req, res

let error_handler {error_handlers; _} next (req, res) =
  (* Forking here and creating a switch allows us to catch errors after the response has started *)
  let promise, resolver = Promise.create () in

  Fiber.fork ~sw:req.sw (fun () ->
    try
    (* I do not know how heavy switches are. I hope not very heavy, because this way can create two for common middleware paths *)
    switch_with_req_lifetime ~name:("error_handler: "^req.resource) req (fun req ->
      Promise.resolve resolver (next (req, res))
    )
    with
    | User_disconnect -> (
      if not (Promise.try_resolve resolver (req, {res with status = `I_m_a_teapot; body = String ""})) then
        Logs.debug (fun m -> m "%s: connection reset" req.resource)
    )
    | Bad_request -> (
      Logs.debug (fun m -> m "Bad request: %s" (Printexc.get_backtrace ()));
      if Promise.is_resolved promise then
        Logs.err (fun m -> m "%s: client error (Bad_request) after first byte?" req.resource)
      else
        Promise.resolve resolver (error_handlers.bad_request (req, res))
    )
    | Http_not_found -> (
      Logs.debug (fun m -> m "Not found: %s" (Printexc.get_backtrace ()));
      if Promise.is_resolved promise then
        Logs.err (fun m -> m "%s: client error (Not_found) after first byte?" req.resource)
      else
        Promise.resolve resolver (error_handlers.not_found (req, res))
    )
    | exn -> (
      if Promise.is_resolved promise then (
        Logs.info (fun m -> m "%s: error caught after first byte - %s" req.resource (Printexc.to_string exn));
        Logs.debug (fun m -> m "%s" (Printexc.get_backtrace ()))
      ) else
        Promise.resolve resolver (error_handlers.internal_server_error exn (req, res))
    )
  );

  Promise.await promise

let processing_timeout timeout app next (req, res) =
  Fiber.first ~combine:(fun (a, b) c -> if b.status = `Internal_server_error then a, b else c)
    (fun () -> timeout (); app.error_handlers.timeout (req, res))
    (fun () -> next (req, res))

let cancel_on_reset _ next (req, res) =
  let promise, resolver = Promise.create () in

  Fiber.fork ~sw:req.sw (fun () ->
    switch_with_req_lifetime ~name:("cancel_on_reset: "^req.resource) req (fun req ->
      Fiber.fork_daemon ~sw:req.sw (fun () ->
        while req.poll_readable () do
          Fiber.yield ()
        done;
        Logs.debug (fun m -> m "Socket is no longer readable - user most likely disconnected, lets fail");
        Switch.fail req.sw User_disconnect;
        `Stop_daemon
      );
      Promise.resolve resolver (next (req, res));
    )
  );

  Promise.await promise

(* does not replace user-set headers, so can be disabled through setting cache-control: no-cache *)
let cache_control ?(max_age = 30) _ =
  let open Httpaf in
  let value = sprintf "max-age=%d" max_age in
  fun next (req, res) ->
    let req, res = next (req, res) in
    req,
    if req.meth = `GET && Status.is_successful res.status then {res with headers = Headers.add_unless_exists res.headers "cache-control" value} else res

let forwarded_for ?(trusted = [Net.Ipaddr.V4.loopback]) _ next ((req:request), res) =
  let open Httpaf in
  match Headers.get req.headers "x-forwarded-for" with
  | None -> next (req, res)
  | Some forwarded ->
    match List.find_opt ((=) req.ip) trusted with
    | None -> raise Bad_request
    | Some _ ->
      let ip = try
        Net.Ipaddr.of_raw forwarded
      with Invalid_argument _ -> raise Bad_request in

      next ({req with headers = Headers.remove req.headers "x-forwarded-for"; ip}, res)


(*
 * demo middlewares
 *)
let demo_router routes {error_handlers; _} next (req, res) =
  match List.assoc_opt req.resource routes with
  | None -> next (error_handlers.not_found (req, res))
  | Some handler -> next (handler (req, res))

let cowsay _ next (req, res) =
  let req, res = next (req, res) in
  match res.body with
  | Stream _ -> req, res (* middleware can't do anything to streams *)
  | String body ->
  req, { res with body = String (sprintf "
  %s
   \\
    \\
    ^__^ 
    (oo)\\_______
    (__)\       )\\/\\
        ||----w |
        ||     ||

" body
  )}


(* app templates and construction methods
 * middleware is constructed backwards
 *)

let use middleware app =
  {app with middlewares = middleware :: app.middlewares}

let not_found handler app =
  {app with error_handlers = {app.error_handlers with not_found = handler}}

let timeout handler app =
  {app with error_handlers = {app.error_handlers with timeout = handler}}

let method_not_allowed handler app =
  {app with error_handlers = {app.error_handlers with method_not_allowed = handler}}

let bad_request handler app =
  {app with error_handlers = {app.error_handlers with bad_request = handler}}

(** Set the handler called on uncaught exception
 *  {b Handlers may assume they are running inside of an exception handler, and any exception handling code should honour this}
 *)
let internal_server_error handler app =
  {app with error_handlers = {app.error_handlers with internal_server_error = handler}}

let barebones = {
  middlewares = [];
  error_handlers = {
    not_found = (fun (req, res) -> req, {res with status = `Not_found; body = String "Not Found."});
    method_not_allowed = (fun (req, res) -> req, {res with status = `Method_not_allowed; body = String "Method Not Allowed."});
    bad_request = (fun (req, res) -> req, {res with status = `Bad_request; body = String "Malformed Request."});
    internal_server_error = (fun exn (req, res) ->
      let exn = Printexc.to_string exn in
      Logs.err (fun m -> m "%s: error caught - %s\n%s" req.resource exn (Printexc.get_backtrace ()));
      req, {res with status = `Internal_server_error; body = String "Internal Server Error."}
    );
    timeout = (fun (req, res) -> req, {res with status = `Bad_gateway; body = String "Internal Timeout."})
  }
}

let debug_error_handler exn (req, res) =
  let exn = Printexc.to_string exn in
  let bt = Printexc.get_backtrace () in
  Logs.err (fun m -> m "%s: error caught - %s\n%s" req.resource exn bt);
  Content_type.negotiate req.accepts [
    (Text, Plain), (fun () -> req, {res with status = `Internal_server_error; body = String (sprintf "%s\n%s" exn bt)});
    (Application, Json), (fun () -> req, {
      status = `Internal_server_error; headers = Httpaf.Headers.add res.headers "content-type" "application/json; charset=utf-8"; body = String (sprintf {|{
        "error": "internal.server.error",
        "message": "%s",
        "backtrace": "%s"
      }|} exn bt)
    });
    (Text, Html), (fun () -> req, {
      status = `Internal_server_error; headers = Httpaf.Headers.add res.headers "content-type" "text/html; charset=utf-8"; body = String (sprintf {|
      <!DOCTYPE html>
      <html>
      <head>
      <title>🐫💀 Error handler</title>
      </head>
      <body>
      <p>Zimmerman debug error handler</p>
      <h1>OCaml Exception!</h1>
      <h2>%s</h2>
      <pre>%s</pre>
      |} exn bt)
    })
  ]

let default ?(dump_errors = false) () =
  let app =
    barebones
    |> use logger
    |> use error_handler
    |> use forwarded_for
    |> use cache_control
  in
  if dump_errors then
    internal_server_error debug_error_handler app
  else
    app


(* HTTP *)
open Httpaf

let on_eof reqd middlewares conn sw ip body =
  let Request.{target; meth; headers; _} = Reqd.request reqd in

  let dummy_buf = Cstruct.create 1 in

  let body_finished, bf_resolver = Promise.create () in

  let _, res = execute_middlewares middlewares
    ({time = Unix.gettimeofday ();
      resource = target;
      meth;
      poll_readable = (fun () ->
        Logs.debug (fun m -> m "poll_readable");
        match Flow.single_read conn dummy_buf with
        | _ -> true
        | exception End_of_file -> false (* direct to client *)
        | exception Io (Net.E (Connection_reset _), _) -> Logs.debug (fun m -> m "eio threw connection reset"); false); (* http mode reverse proxy *)
      sw;
      headers;
      body;
      body_finished;
      ip;
      accepts = Content_type.of_string (Headers.get headers "accept" |> Option.value ~default:"*/*")
     }, {
      status = `Not_found;
      body = String "Not Found.";
      headers = Headers.empty
    }) in

  Logs.debug (fun m -> m "Response recieved");

  let response = Response.create ~headers:(
    let headers = Headers.add_unless_exists res.headers "content-type" "text/plain; charset=utf-8" in
    if Headers.mem headers "connection" || Headers.mem headers "transfer-encoding" then
      headers
    else
      match res.body with
      | String s -> Headers.add_unless_exists (Headers.add_unless_exists headers "connection" "keep-alive") "content-length" (string_of_int (String.length s))
      | Stream _ -> Headers.add_unless_exists headers "transfer-encoding" "chunked"
  ) res.status in

  match res.body with
  | String body ->
    Reqd.respond_with_string reqd response body;
    Promise.resolve bf_resolver ()
  | Stream stream ->
    let writer = Reqd.respond_with_streaming reqd response in
    let rec write =
      function
      | None -> Body.Writer.close writer; Promise.resolve bf_resolver ()
      | Some chunk ->
        Body.Writer.write_string writer chunk;
        Body.Writer.flush writer (fun () -> Logs.debug (fun m -> m "Flush"); write (Stream.take stream))
    in write (Stream.take stream)

let rec on_read body buf on_eof bigstring ~off ~len =
  Buffer.add_string buf (Bigstringaf.substring ~off ~len bigstring);
  Body.Reader.(schedule_read body ~on_eof:(fun () -> close body; on_eof (Some (Buffer.contents buf))) ~on_read:(on_read body buf on_eof))


(** Start listening for requests. Takes most of the same arguments as Eio.Net.run_server
  * {b additional_domains enables beta multicore support (see Eio.Net.run_server).
  * This has not yet been fully tested and there may be some thread unsafe code left in default middleware (especially around logs).}
 *)
let serve ?additional_domains ?(parse_proxyproto = []) ?stop ~sw socket (app:app) =
  let middlewares = List.rev_map (fun middleware -> middleware app) app.middlewares in
  if additional_domains <> None then Logs_threaded.enable ();
  Logs.info (fun m -> m "Starting server");

  Net.run_server ?additional_domains ?stop ~on_error:(fun e -> Logs.err (fun m -> m "%s" (Printexc.to_string e))) socket @@ fun conn addr ->
  let ip = (
    match addr with
    | `Tcp (ip, _) -> (
      match List.find_opt ((=) ip) parse_proxyproto with
      | None -> ip
      | Some _ ->
        match String.split_on_char ' ' Buf_read.(of_flow ~max_size:108 conn |> line) with
        | ["PROXY"; proto; _source; dest; _source_port; _dest_port] -> (
          if proto = "TCP4" || proto = "TCP6" then
            try Net.Ipaddr.of_raw dest with Invalid_argument _ -> failwith "malformed tcp proxy header IP"
          else failwith "unknown proxy header protocol"
        )
        | _ -> failwith "malformed tcp proxy header"
    )
    | _ -> failwith "excepted ip"
  ) in

  Httpaf_eio.Server.create_connection_handler ~sw
    ~error_handler:(fun _ ?request:_ err _ ->
      match err with
      | `Exn exn -> raise exn
      | `Bad_request -> (
        Logs.err @@ fun m -> m "Error caught (httpaf): Bad_request";
        Printexc.print_backtrace stdout;
        print_newline ()
      )
      | _ -> Logs.err @@ fun m -> m "Error caught (httpaf)"
    )
    ~request_handler:(fun _ {reqd; _} -> Switch.run @@ fun sw -> (* switches are per-domain, and run_server might have moved us to another one *)
      let req = Reqd.request reqd in
      let on_eof = on_eof reqd middlewares conn sw ip in
      let content_length = Option.bind (Headers.get req.headers "content-length") (fun v -> int_of_string_opt v) |> Option.value ~default:0 in
      match req.meth with
      | (`POST | `PUT | `DELETE) when content_length > 0 -> (
        if content_length > Sys.max_string_length then
          Reqd.respond_with_string reqd (Response.create `Payload_too_large ~headers:(Headers.of_list ["connection", "close"])) "Body too large"
        else
        let body = Reqd.request_body reqd in
        let buf = Buffer.create content_length in
        Body.Reader.(schedule_read body ~on_eof:(fun () -> close body; on_eof (Some (Buffer.contents buf))) ~on_read:(on_read body buf on_eof))
      )
      |_ -> on_eof None
    )
    addr conn