(* Implements content negotiation *)

module String = CCString

(* common mimetypes *)
type mime_type = Text | Application | Image | Any | Other of string
type mime_subtype = Html | Json | Xhtml | Xml | Webp | Jpeg | Png | Plain | Any | Other of string

type accepts = (mime_type * mime_subtype) list

let split_accept = Re.Perl.re ", ?" |> Re.compile |> Re.split
let parse_substr = Re.Perl.re "^([A-z]+)\\/([A-z+]+)(?:;q=.+)?$" |> Re.compile

(* TODO: quality value syntax *)
let of_string header: accepts =
  if String.is_empty header then [Any, Any]
  else split_accept header |> List.filter_map (fun fragment ->
    match Re.exec_opt parse_substr fragment with
    | None -> None
    | Some groups ->
      let mime_type: mime_type =
        match Re.Group.get groups 1 with
        | "text" -> Text
        | "application" -> Application
        | "image" -> Image
        | "*" -> Any
        | mime -> Other mime
      in

      let subtype: mime_subtype =
        match Re.Group.get groups 2 with
        | "html" -> Html
        | "json" -> Json
        | "xhtml+xml" -> Xhtml
        | "xml" -> Xml
        | "webp" -> Webp
        | "png" -> Png
        | "*" -> Any
        | mime -> Other mime
      in

      Some (mime_type, subtype)
  )

(*
 * Falls back to first item
 *)
let rec negotiate (accepts: accepts) provides =
  if List.is_empty provides then raise (Invalid_argument "empty array passed to negotiate");

  match accepts with
  | [] -> let _, handler = List.hd provides in handler ()
  | (acpt, acpt_sub) :: tl ->
    match
      List.find_map (fun ((prov, prov_sub), handler) ->
        if (acpt = Any || acpt = prov) && (acpt_sub = Any || acpt_sub = prov_sub) then Some handler
        else None
      ) provides
    with
    | Some handler -> handler ()
    | None -> negotiate tl provides