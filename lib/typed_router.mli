type matches

module Path : sig
  type params = matches list

  val string: params -> int -> string

  val int: params -> int -> int

  val bool: params -> int -> bool
end

module Query : sig
  type queries

  val get: queries -> string -> matches

  val string: queries -> string -> string
  val string_opt: queries -> string -> string option

  val int: queries -> string -> int
  val int_opt: queries -> string -> int option

  val bool: queries -> string -> bool
  val bool_opt: queries -> string -> bool option

  val raw: queries -> (string * string) list
end

type router

val create : ?error_handlers:Core.error_handlers -> string -> router
val use : Core.middleware -> router -> router
val register :
  [< `GET | `POST ] ->
  string ->
  ?additional_middleware:Core.middleware list ->
  (params:matches list ->
   queries:Query.queries ->
   Core.request * Core.response -> Core.request * Core.response) ->
  router -> router

val get :
  string ->
  ?additional_middleware:Core.middleware list ->
  (params:matches list ->
   queries:Query.queries ->
   Core.request * Core.response -> Core.request * Core.response) ->
  router -> router

val post :
  string ->
  ?additional_middleware:Core.middleware list ->
  (params:matches list ->
   queries:Query.queries ->
   Core.request * Core.response -> Core.request * Core.response) ->
  router -> router

val put :
  string ->
  ?additional_middleware:Core.middleware list ->
  (params:matches list ->
   queries:Query.queries ->
   Core.request * Core.response -> Core.request * Core.response) ->
  router -> router

val delete :
  string ->
  ?additional_middleware:Core.middleware list ->
  (params:matches list ->
   queries:Query.queries ->
   Core.request * Core.response -> Core.request * Core.response) ->
  router -> router

val any :
  string ->
  ?additional_middleware:Core.middleware list ->
  (params:matches list ->
   queries:Query.queries ->
   Core.request * Core.response -> Core.request * Core.response) ->
  router -> router

val middleware :
  router ->
  Core.app ->
  (Core.request * Core.response -> 'a) -> Core.request * Core.response -> 'a
