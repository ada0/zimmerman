open Eio

type response_body =
| String of string
| Stream of string option Stream.t

type request = {
  time: float;
  resource: string;
  poll_readable: unit -> bool;
  meth: Httpaf.Method.t;
  sw: Switch.t;
  headers: Httpaf.Headers.t;
  body: string option;
  body_finished: unit Promise.t;
  ip: Net.Ipaddr.v4v6;
  accepts: Content_type.accepts;
}

type response = {
  status: Httpaf.Status.t;
  body: response_body;
  headers: Httpaf.Headers.t;
}

type handler = request * response -> request * response

(* common record for error responses *)
type error_handlers = {
  not_found: handler;
  method_not_allowed: handler;
  bad_request: handler;
  internal_server_error: exn -> handler;
  timeout: handler;
}

(* all middleware take an app argument to allow for the app to be constructed then passed fully-formed when the server starts *)
type app = {
  middlewares: middleware list;
  error_handlers: error_handlers;
}
and middleware = app -> handler -> request * response -> request * response

type real_middleware = handler -> request * response -> request * response

let rec execute_middlewares middlewares reqres =
  match middlewares with
  | [hd] -> hd (fun reqres -> reqres) reqres
  | hd :: tl -> hd (execute_middlewares tl) reqres
  | [] -> reqres

let simulate_request ~sw app resource =
  let _, res = execute_middlewares app
    ({time = Unix.gettimeofday (); resource; poll_readable = (fun () -> true); meth = `GET; sw; headers = Httpaf.Headers.empty; body = None; body_finished = Promise.create_resolved (); ip = Net.Ipaddr.V4.loopback; accepts = [Any, Any]},
     {status = `Not_found; body = String "Not Found."; headers = Httpaf.Headers.of_list ["connection", "close"]}) in
  res

let dump_response response =
  Printf.printf "HTTP %d\n\n%s%!" (Httpaf.Status.to_code response.status) (
    match response.body with
    | String body -> body
    | Stream stream ->
      let buf = Buffer.create 128 in
      let rec write =
        function
        | None -> Buffer.contents buf
        | Some chunk -> Buffer.add_string buf chunk; write (Stream.take stream)
      in write (Stream.take stream)
  )

exception Bad_request
exception User_disconnect
exception Http_not_found