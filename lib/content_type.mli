type mime_type = Text | Application | Image | Any | Other of string
type mime_subtype = Html | Json | Xhtml | Xml | Webp | Jpeg | Png | Plain | Any | Other of string
type accepts = (mime_type * mime_subtype) list

val of_string: string -> accepts
val negotiate: accepts -> ((mime_type * mime_subtype) * (unit -> 'a)) list -> 'a