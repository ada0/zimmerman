module Core = Core
type request = Core.request
type response = Core.response

module Typed_router = Typed_router
module Content_type = Content_type

exception User_disconnect
exception Bad_request
exception Http_not_found

val switch_with_req_lifetime: ?name:string -> Core.request -> (Core.request -> unit) -> unit
val logger : Core.middleware
val error_handler : Core.middleware
val processing_timeout : (unit -> unit) -> Core.middleware
val cancel_on_reset : Core.middleware
val demo_router : (string * Core.handler) list -> Core.middleware
val cowsay : Core.middleware
val use : Core.middleware -> Core.app -> Core.app
val not_found : Core.handler -> Core.app -> Core.app
val timeout : Core.handler -> Core.app -> Core.app
val method_not_allowed : Core.handler -> Core.app -> Core.app
val bad_request : Core.handler -> Core.app -> Core.app
val internal_server_error : (exn -> Core.handler) -> Core.app -> Core.app
val barebones : Core.app
val debug_error_handler :
  exn -> Core.request * Core.response -> Core.request * Core.response
val default : ?dump_errors:bool -> unit -> Core.app
val serve :
  ?additional_domains: Eio.Domain_manager.ty Eio.Resource.t * int ->
  ?parse_proxyproto:Eio.Net.Ipaddr.v4v6 list ->
  ?stop:unit Eio.Promise.t ->
  sw:Eio.Switch.t ->
  [> [> `Generic ] Eio.Net.listening_socket_ty ] Eio.Resource.t ->
  Core.app ->
  unit
