open Core
open Printf

module String = CCString

(* Core parsing and testing *)
type matches = Str of string | Int of int | Bool of bool

type toggleable = (* for booleans *)
| Mandatory (* fail if not true/false/1/0/yes/no *)
| Optional (* Stricter than toggleable: Some if valid, None if not provided, error if any input is invalid *)
| Toggleable (* true if provided with any non-false value, false otherwise *)
type optional = (* for everything else *)
| Optional
| Mandatory

type conversions = Constant of string | Boolean of toggleable | String of optional | Integer of optional

let check_for_typo = Re.Perl.re "string|int|bool" |> Re.compile

module Path = struct
  type pattern = conversions list
  type params = matches list

  let compile string: pattern =
    List.map (function
    | ":bool" -> Boolean Mandatory
    | ":string" -> String Mandatory
    | ":int" -> Integer Mandatory
    | literal ->
      (* running Re.matches inside Logs.info causes a crash with Logs_threaded *)
      begin match Logs.level () with
      | None | Some (App | Error | Warning) -> ()
      | _ ->
        match Re.matches check_for_typo literal with
        | [typo] -> Logs.info (fun m -> m "Warning: you typed %s; did you mean :%s?" typo typo)
        | _ -> ()
      end;
      Constant literal
    ) (String.split_on_char '/' string)

  let segments path =
    List.filter ((<>) "") (String.split_on_char '/' path)

  let test pattern segments: params option =
    try Some (
      List.fold_left2 (fun params segment -> 
        function
        | Constant literal -> if segment = literal then params else raise Not_found
        | String _ -> Str segment :: params
        | Integer _ -> Int (int_of_string segment) :: params
        | Boolean _ ->
          match String.lowercase_ascii segment with
          | "true" | "1" | "yes" -> Bool true :: params
          | "false" | "0" | "no" -> Bool false :: params
          | _ -> raise Not_found
      ) [] segments pattern
      |> List.rev
    ) with Invalid_argument _ | Not_found | Failure _ -> None

  let string (params: params) index =
    match List.nth params index with
    | Str s -> s
    | _ -> raise (Invalid_argument (sprintf "param %d is not a string" index))

  let int (params: params) index =
    match List.nth params index with
    | Int s -> s
    | _ -> raise (Invalid_argument (sprintf "param %d is not a int" index))
    
  let bool (params: params) index =
    match List.nth params index with
    | Bool s -> s
    | _ -> raise (Invalid_argument (sprintf "param %d is not a bool" index))
end

let or_bad_req =
  function
  | None -> raise Bad_request
  | Some v -> v
  
let int_of_string_nf s =
  match int_of_string_opt s with
  | Some i -> i
  | None -> raise Not_found

module Query = struct
  type pattern = (string * conversions) list
  type queries = {
    matches: (string * matches option) list;
    elements: (string * string) list;
  }

  let compile string =
    List.map (fun el ->
      match String.Split.left ~by:"=" el with
      | None -> el, Boolean Toggleable
      | Some (key, ":bool!") -> key, Boolean Mandatory
      | Some (key, ":bool") -> key, Boolean Optional
      | Some (key, ":string!") -> key, String Mandatory
      | Some (key, ":string") -> key, String Optional
      | Some (key, ":int!") -> key, Integer Mandatory
      | Some (key, ":int") -> key, Integer Optional
      | Some (key, literal) ->
        Logs.info (fun m ->
          match Re.matches check_for_typo literal with
          | [typo] -> m "Warning: you typed %s; did you mean :%s" typo typo
          | _ -> ()
        );
        key, Constant literal
    ) (String.split_on_char '&' string)

  let parse string =
    List.map (fun el ->
      match String.Split.left ~by:"=" el with
      | Some (k, v) -> k, Uri.pct_decode v
      | None -> el, ""
    ) (String.split_on_char '&' string)

  let test pattern elements =
    try Some (
      let matches = List.fold_left (fun queries -> function
      | key, Constant literal -> if List.assoc key elements = literal then queries else raise Not_found
      | key, String Optional -> (key, Option.bind (List.assoc_opt key elements) (fun value -> Some (Str value))) :: queries
      | key, String Mandatory -> (
        let value = List.assoc key elements in
        if String.is_empty value then raise Not_found
        else (key, Some (Str value)) :: queries
      )
      | key, Integer Optional -> (
        key, Option.bind
          (List.assoc_opt key elements) (fun value -> Option.bind (int_of_string_opt value) (fun value -> Some (Int value)))
      ) :: queries
      | key, Integer Mandatory -> (key, Some (Int (int_of_string_nf (List.assoc key elements)))) :: queries
      | key, Boolean Toggleable -> (
        match Option.bind (List.assoc_opt key elements) (fun s -> Some (String.lowercase_ascii s)) with
        | Some ("true" | "1" | "yes" | "") -> key, Some (Bool true)
        | _ -> key, Some (Bool false)
      ) :: queries
      | key, Boolean Mandatory -> (
        match List.assoc key elements |> String.lowercase_ascii with
        | "true" | "1" | "yes" -> key, Some (Bool true)
        | "false" | "0" | "no" -> key, Some (Bool false)
        | _ -> raise Not_found
      )  :: queries
      | key, Boolean Optional -> (
        match Option.bind (List.assoc_opt key elements) (fun s -> Some (String.lowercase_ascii s)) with
        | None -> key, None
        | Some ("true" | "1" | "yes") -> key, Some (Bool true)
        | Some ("false" | "0" | "no") -> key, Some (Bool false)
        | _ -> raise Not_found
      ) :: queries
      ) [] pattern in
      {matches; elements}
    ) with Not_found -> None

  let get_opt (queries:queries) key =
    match List.assoc_opt key queries.matches with
    | Some (Some v) -> Some v
    | _ -> None
  let get queries key = or_bad_req (get_opt queries key)

  let string_opt queries key =
    match get_opt queries key with
    | Some (Str v) -> Some v
    | _ -> None
  let string queries key = or_bad_req (string_opt queries key)

  let int_opt queries key =
    match get_opt queries key with
    | Some (Int v) -> Some v
    | _ -> None
  let int queries key = or_bad_req (int_opt queries key)
  
  let bool_opt queries key =
    match get_opt queries key with
    | Some (Bool v) -> Some v
    | _ -> None
  let bool queries key = or_bad_req (bool_opt queries key)

  let raw queries = queries.elements
end

let split_path path =
  let path = if path.[0] = '/' then String.drop 1 path else path in
  match String.Split.left ~by:"?" path with
  | Some s -> s
  | None -> path, ""

let compile path =
  let path, query = split_path path in
  Path.compile path, Query.compile query

let parse path =
  let path, query = split_path path in
  Path.segments path, Query.parse query


(* middleware *)
type handler_and_middleware = {
  middlewares: middleware list;
  handler: params:matches list -> queries:Query.queries -> request * response -> request * response;
}

type handler_and_middleware_real = {
  middlewares: real_middleware list;
  handler: params:matches list -> queries:Query.queries -> request * response -> request * response;
}

type method_handlers = (Query.pattern, handler_and_middleware) Hashtbl.t
type methods = {
  get: method_handlers;
  post: method_handlers;
  put: method_handlers;
  delete: method_handlers;
}

type method_handlers_iterable = (Query.pattern * handler_and_middleware_real) list
type methods_iterable = {
  get: method_handlers_iterable;
  post: method_handlers_iterable;
  put: method_handlers_iterable;
}

type router = {
  prefix: string;
  middlewares: middleware list;
  routes: (Path.pattern, methods) Hashtbl.t;
  error_handlers: error_handlers option;
}

let create ?error_handlers prefix = {
  prefix;
  middlewares = [];
  routes = Hashtbl.create 2;
  error_handlers;
}

let use middleware router = {router with middlewares = middleware :: router.middlewares}

let register meth pattern ?(additional_middleware = []) handler router =
  let path, query = compile pattern in
  let middleware: handler_and_middleware = {
    middlewares = additional_middleware;
    handler
  } in
  let methods =
    match Hashtbl.find_opt router.routes path with
    | Some methods -> methods
    | None -> 
      let methods: methods = {get = Hashtbl.create 1; post = Hashtbl.create 1; put = Hashtbl.create 1; delete = Hashtbl.create 1} in
      Hashtbl.add router.routes path methods;
      methods
  in
  
  let hashtbl =
    match meth with
    | `GET -> methods.get
    | `POST -> methods.post
    | `PUT -> methods.put
    | `DELETE -> methods.delete
  in
  if Hashtbl.find_opt hashtbl query <> None then failwith "Duplicate route" else Hashtbl.add hashtbl query middleware;
  router

let get = register `GET
let post = register `POST
let put = register `PUT
let delete = register `DELETE

let any pattern ?additional_middleware handler router =
  register `GET pattern ?additional_middleware handler router
  |> register `POST pattern ?additional_middleware handler
  |> register `PUT pattern ?additional_middleware handler
  |> register `DELETE pattern ?additional_middleware handler

let rec execute_route (ham:handler_and_middleware_real) params queries reqres =
  match ham.middlewares with
  | [hd] -> hd (ham.handler ~params ~queries) reqres
  | hd :: tl -> hd (execute_route {middlewares = tl; handler = ham.handler} params queries) reqres
  | [] -> ham.handler ~params ~queries reqres

let middleware router app =
  let finalize_middleware hashtbl =
    Hashtbl.fold (fun pattern (ham: handler_and_middleware) (acc: (Query.pattern * handler_and_middleware_real) list) ->
      (pattern, {handler = ham.handler; middlewares = List.map (fun middleware -> middleware app) ham.middlewares}) :: acc
    ) hashtbl []
  in
  let routes = Hashtbl.fold (fun path (methods:methods) acc ->
    (path, {get = finalize_middleware methods.get; post = finalize_middleware methods.post; put = finalize_middleware methods.put}) :: acc
  ) router.routes [] in

  let error_handlers = Option.value ~default:app.error_handlers router.error_handlers in

  fun next (req, res) ->
  if String.prefix ~pre:router.prefix req.resource then
    let path, query = parse (String.drop (String.length router.prefix) req.resource) in
    let handler = List.find_map (fun (path_pat, methods) ->
      match Path.test path_pat path with
      | None -> None
      | Some params ->
        let patterns = match req.meth with `GET -> methods.get | `POST -> methods.post | `PUT -> methods.put |_ -> [] in
        if List.is_empty patterns then Some error_handlers.method_not_allowed
        else Some (
          List.find_map (fun (query_pat, ham) ->
            match Query.test query_pat query with
            | None -> None
            | Some queries -> Some (execute_route ham params queries)
          ) patterns |> Option.value ~default:error_handlers.bad_request
        )
    ) routes |> Option.value ~default:error_handlers.not_found in
    next (handler (req, res))
  else next (req, res)