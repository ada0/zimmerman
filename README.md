# Zimmerman
Zimmerman is a cascading middleware web framework using [Eio](https://github.com/ocaml-multicore/eio) and [http/af](https://github.com/anmonteiro/httpaf).
```ocaml
let open Zimmerman in
use (fun _app _next (req, res) ->
    req, {res with
        status = `OK;
        body = String "Hello World!"
    }
) default
|> serve ~sw (
    Net.listen
        ~reuse_addr:true
        ~sw
        ~backlog:1
        env#net
        (`Tcp (Net.Ipaddr.V4.any, 1234))
)
```
Documentation can be found [here](https://ada0.codeberg.page/zimmerman/zimmerman/) and an example can be found in [`bin/test.ml`](https://codeberg.org/ada0/zimmerman/src/branch/main/bin/test.ml).

Zimmerman has been most closely inspired by the excellent [Koa](https://koajs.com/), with typed routing inspired by [Rocket](https://rocket.rs/) and cancellation and streaming support taken from my own experiments in writing HTTP code on Eio. Middleware can alter anything about a given transaction, including the response, all in pure direct-style Eio. Haproxy's proxy-protocol standard is supported.

It is very early along, and is likely to be quite buggy and change often. Issues and pull requests welcome!

The goal is to build a low-boilerplate, flexible framework for concurrent HTTP applications.

## Builtin middleware
- Typed_router: A Rocket-style router that can match and convert URL parameters and queries
- logger: Does what it says on the tin
- error_handler: Handles exceptions, including those representing client errors. Uses user provided error handlers from app.error_handlers. (Simple ones are provided, including debug_error_handler to display backtraces in development)
- demo_router: The simplest possible router, intended as an example middleware more than anything.
- processing_timeout: Takes a function, which will cancel the handler after returning.
- forwarded_for: Parse X-Forwarded-For from trusted ips and replace req.ip
- cancel_on_reset (experimental):
  Attempts to detect user disconnects and cancel the handler.
  This middleware replaces req.sw with a fresh switch, then polls the client to see if they're still connected; use Switch.run_protected to exempt individual functions from this
  In a direct connection or behind a tcp reverse proxy, req.sw will be cancelled immediately even before response headers are sent.
  Behind a http reverse proxy, req.sw will be cancelled if the client disconnects halfway through chunked streaming, but slower.
- cowsay: cowsay!

## TODO
Short term:
- Test behind common reverse proxies (Nginx, Apache mod_proxy, Varnish), specifically using cancel_on_reset
- Proper documentation
- Functional testing
- Benchmarking and profiling
- Typed_router ppx for compile-time route checking
- Multipart forms

Long term:
- Make as much code as possible scheduler-agnostic
- Unit testing