[@@@warning "-23"]

open Zimmerman
open Eio
open Format

let () = Logs.set_reporter (Logs_fmt.reporter ()); Logs.set_level (Some Debug)

let () = Eio_main.run @@ fun env -> Switch.run @@ fun sw ->
  let app =
    default ~dump_errors:true ()
    |> use cancel_on_reset
  in

  let cowsay_endpoint ~params:_ ~queries:_ ((req:request), (res:response)) =
    match req.body with
    | None -> raise Bad_request
    | Some message ->
    req, {res with status = `OK; body = String message}
  in

  let router = Typed_router.(
    create ""
    |> any "/hello/:string?exclaim" ~additional_middleware:[cowsay] (fun ~params ~queries (req, res) ->
      req, {res with status = `OK; body = String (asprintf "Hello to %s at %a%s" (Path.string params 0) Net.Ipaddr.pp req.ip (if Query.bool queries "exclaim" then "!" else ""))}
    )
    |> get "/timeout" ~additional_middleware:[processing_timeout (fun () -> Time.sleep env#clock 5.)] (fun ~params:_ ~queries:_ (req, res) ->
      Time.sleep env#clock 10.;
      req, {res with status = `OK; body = String "This Should Not Be Seen."}
    )
    |> get "/slow-stuff" (fun ~params:_ ~queries:_ (req, res) ->
      Time.sleep env#clock 10.;
      req, {res with status = `OK; body = String "This Should Be Seen!"}
    )
    |> get "/stream?chunks=:int" (fun ~params:_ ~queries (req, res) ->
      let chunks = Query.int_opt queries "chunks" |> Option.value ~default:2 in
      let stream = Stream.create max_int in
      let content_type = Content_type.negotiate req.accepts [
        (Text, Plain), (fun () ->
          Fiber.fork ~sw:req.sw (fun () ->
            for i = 1 to chunks do
              Time.sleep env#clock 2.;
              Logs.info (fun m -> m "Pushing chunk");
              Stream.add stream (Some (sprintf "Chunk %d\n" i))
            done;
            Stream.add stream None
          );
          "text/plain; charset=utf-8"
        );
        (Text, Html), (fun () ->
          Stream.add stream (Some "<!DOCTYPE html><html><head><title>Streaming demo</title></head><body><h1>Streaming demo</h1>");
          Fiber.fork ~sw:req.sw (fun () ->
            for i = 1 to chunks do
              Time.sleep env#clock 2.;
              Logs.info (fun m -> m "Pushing chunk");
              Stream.add stream (Some (sprintf "<p>Chunk %d</p>" i))
            done;
            Stream.add stream (Some "<span style='background: linear-gradient(to right, #ef5350, #f48fb1, #7e57c2, #2196f3, #26c6da, #43a047, #eeff41, #f9a825, #ff5722)'>All done!</span></body></html>");
            Stream.add stream None
          );
          "text/html; charset=utf-8"  
        )
      ] in
      req, {res with status = `OK; body = Stream stream; headers = Httpaf.Headers.add res.headers "content-type" content_type}
    )
    |> get "/error" (fun ~params:_ ~queries:_ _ ->
      failwith "Oops! Can you see this error?"
    )
    |> get "/redirect-me?target=:string!" (fun ~params:_ ~queries (req, res) ->
      req, {res with status = `Temporary_redirect; headers = Httpaf.Headers.add res.headers "location" (Query.string queries "target")}
    )
    |> post "/cowsay" ~additional_middleware:[cowsay] cowsay_endpoint
    |> put "/cowsay" ~additional_middleware:[cowsay] cowsay_endpoint
    |> post "/json" (fun ~params:_ ~queries:_ (req, res) ->
      match req.body with
      | None -> raise Bad_request
      | Some body -> Yojson.(
        let body = try Basic.from_string body with Json_error _ -> raise Bad_request in
        let open Basic.Util in
        let message = member "message" body |> try to_string with Type_error _ -> raise Bad_request in
        let use_cowsay = try to_bool_option (member "cowsay" body) |> Option.value ~default:false with Type_error _ -> raise Bad_request in
        if use_cowsay then cowsay app (fun reqres -> reqres) (req, {res with status = `OK; body = String message})
        else (req, {res with status = `OK; body = String message})
      )
    )
    |> get "/json?payload=:string!" (fun ~params:_ ~queries (req, res) -> Yojson.(
      let body = try Basic.from_string (Query.string queries "payload") with Json_error _ -> raise Bad_request in
      let open Basic.Util in
      let message = member "message" body |> try to_string with Type_error _ -> raise Bad_request in
      let use_cowsay = try to_bool_option (member "cowsay" body) |> Option.value ~default:false with Type_error _ -> raise Bad_request in
      if use_cowsay then cowsay app (fun reqres -> reqres) (req, {res with status = `OK; body = String message})
      else (req, {res with status = `OK; body = String message})
    ))
  ) in
  
  serve ~additional_domains:(env#domain_mgr, Domain.recommended_domain_count ()) ~sw
    (Net.listen ~reuse_addr:true ~sw ~backlog:1 env#net (`Tcp (Net.Ipaddr.V4.any, 1234))) (use (Typed_router.middleware router) app)
